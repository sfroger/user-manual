Installation
============

Install packages
----------------

Archlinux
^^^^^^^^^

.. code-block:: bash

   yaourt -S fusiondirectory-plugin-ldapmanager
   
Debian
^^^^^^

.. code-block:: bash

   apt-get install fusiondirectory-plugin-ldapmanager
   
RHEL
^^^^

.. code-block:: bash

   yum install fusiondirectory-plugin-ldapmanager
   

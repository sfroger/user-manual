Installation
============

Install packages
----------------

Archlinux
^^^^^^^^^

.. code-block:: bash

   yaourt -S fusiondirectory-plugin-developers
   
Debian
^^^^^^

.. code-block:: bash

   apt-get install fusiondirectory-plugin-developers
   
RHEL
^^^^

.. code-block:: bash

   yum install fusiondirectory-plugin-developers

Installation
============

Install packages
----------------

Archlinux
^^^^^^^^^

.. code-block:: bash

   yaourt -S fusiondirectory-plugin-certificates
      
Debian
^^^^^^

.. code-block:: bash

   apt-get install fusiondirectory-plugin-certificates

RHEL
^^^^

.. code-block:: bash

   yum install fusiondirectory-plugin-certificates
   


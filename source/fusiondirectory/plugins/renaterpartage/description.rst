Description
===========

The RENATER Partage plugin manages the mail component of the Partage de RENATER <https://partage.renater.fr/ systems, used by the research and education community in France.

To use it you need to be a Renater Partage partner and have a valid contract with a domain name and API key. 

Distribution and PHP support Policy
===================================

Distribution OSes have different interpretations of what a 'supported version' is, here are the OS and PHP versions FusionDirectory support.

Server OSes 
-----------

 * Debian: stable and oldstable
 * Ubuntu: the two latest LTS releases
 * Enterprise Linux (RHEL, CentOS, ...): the latest major release

PHP version
===========

* Fusiondirectory 1.4 need PHP 7.0


.. include:: /globals.rst

Functionalities
===============

FusionDirectory user template's, you can give the possibility to automatically create some entries during your new user creation process. 

The concept of templates in FusionDirectory is to allow you to automatically create any objects stored inside Fusiondirectory in a programmable way.

    * Define precisely how the attributes will be constructed, uppercase, lowercase, first letter of an attribute+4 letters of another attribute ...
    * Fill other attributes based on value stored elsewhere
    * Generate random password
    * Calculate date / time for account expiration

and so much more ...
